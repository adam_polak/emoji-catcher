import React, { Component } from "react";
import "./Emoji.css";

class Emoji extends Component {
  render() {
    return (
      <div className="Emoji">
        <img src={this.props.emoji.url} />
        <span className="count">{this.props.emoji.count}</span>
      </div>
    );
  }
}

export default Emoji;
