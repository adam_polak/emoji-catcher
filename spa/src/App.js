import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import { LoginPage } from "./pages/login-page/LoginPage";
import { ReactionsPage } from "./pages/reactions-page/ReactionsPage";
import LoginGooglePage from "./pages/login-google-page/LoginGooglePage";
import httpClient from "./tools/httpClient";
import UserContext from "./contexts/UserContext";
import { LoadingPage } from "./pages/loading-page/LoadingPage";

function App() {
  const loggedOutUser = {
    isLoggedIn: false,
    username: null,
    accessToken: null,
    refreshToken: null
  };

  const [loading, setLoading] = useState(true);

  const [user, setUser] = useState(loggedOutUser);

  const logIn = (username, accessToken, refreshToken) => {
    localStorage.setItem("username", username);
    localStorage.setItem("accessToken", accessToken);
    localStorage.setItem("refreshToken", refreshToken);
    setUser({ isLoggedIn: true, username, accessToken, refreshToken });
  };
  const logOut = () => {
    localStorage.removeItem("username");
    localStorage.removeItem("accessToken");
    localStorage.removeItem("refreshToken");
    setUser(loggedOutUser);
  };

  useEffect(() => {
    async function checkToken() {
      try {
        const valid = await httpClient.isAuthenticatedWithRetry();
        if (!valid) {
          logOut();
        } else if (!user.isLoggedIn) {
          const username = localStorage.getItem("username");
          const accessToken = localStorage.getItem("accessToken");
          const refreshToken = localStorage.getItem("refreshToken");
          logIn(username, accessToken, refreshToken);
        }
      } catch (err) {
        logOut();
      }
      setLoading(false);
    }

    checkToken();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user.isLoggedIn]);

  if (loading) return <LoadingPage />;

  return (
    <div className="App">
      <Router>
        {!user.isLoggedIn ? (
          <>
            <UserContext.Provider value={{ user, logIn, logOut }}>
              <Switch>
                <Route path="/login-callback/keycloak" component={LoginGooglePage} />
                <Route path="/" component={LoginPage} />
              </Switch>
            </UserContext.Provider>
          </>
        ) : (
          <UserContext.Provider value={{ user, logIn, logOut }}>
            <Switch>
              <Route path="/" component={ReactionsPage} />
            </Switch>
          </UserContext.Provider>
        )}
      </Router>
    </div>
  );
}

export default App;
