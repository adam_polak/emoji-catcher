import React, { Component } from "react";
import "./Reaction.css";

export class Reaction extends Component {
  render() {
    return (
      <div className="reaction">
        <img src={this.props.reaction.url} alt={this.props.reaction.url} />
        <span className="count">{this.props.reaction.count}</span>
      </div>
    );
  }
}
