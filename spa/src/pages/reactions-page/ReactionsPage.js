import React, { Component } from "react";
import "./ReactionsPage.css";
import { Reaction } from "./components/reaction/Reaction";
import HttpClient from "../../tools/httpClient";

export class ReactionsPage extends Component {
  constructor() {
    super();

    this.state = {
      reactions: []
    };
  }

  componentDidMount = () => {
    const ws = new WebSocket(
      `${process.env.REACT_APP_WS_URL}?accessToken=${localStorage.getItem(
        "accessToken"
      )}`
    );

    ws.onmessage = message => {
      const event = JSON.parse(message.data);

      const existingReaction = this.state.reactions.findIndex(
        reaction => reaction.emoji === event.emoji
      );

      if (existingReaction !== -1) {
        const newState = [...this.state.reactions];
        newState[existingReaction].count = event.count;

        newState.sort((a, b) => b.count - a.count);

        this.setState({ reactions: newState });
      } else {
        this.setState({ reactions: [...this.state.reactions, event] });
      }
    };

    return HttpClient.getReactions()
      .then(reactions => this.setState({ reactions }))
      .catch(
        this.setState({
          reactions: []
        })
      );
  };

  render() {
    return (
      <div className="reactions">
        {this.state.reactions.map((item, index) => (
          <Reaction reaction={item} key={`reaction-${index}`} />
        ))}
      </div>
    );
  }
}
