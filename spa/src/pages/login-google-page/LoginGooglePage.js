import React, { useContext, useEffect, useState } from "react";
import httpClient from "../../tools/httpClient";
import { withRouter } from "react-router-dom";
import UserContext from "../../contexts/UserContext";

function LoginGooglePage(props) {
  const params = new URLSearchParams(window.location.search);
  const code = params.get("code");

  const [loginFailMessage, setLoginFailMessage] = useState("");

  const { logIn } = useContext(UserContext);

  useEffect(() => {
    async function loginWithKeycloak() {
      try {
        const {
          username,
          accessToken,
          refreshToken
        } = await httpClient.loginWithKeycloak(code);
        logIn(username, accessToken, refreshToken);
        props.history.push("/");
      } catch (err) {
        if (err.response && err.response.status === 404) {
          setLoginFailMessage(`User with a given username does not exist.`);
        } else {
          setLoginFailMessage("Server error.");
        }
      }
    }
    loginWithKeycloak();
  }, [code, logIn, props.history]);

  return loginFailMessage ? (
    <div>
      Failed to login: {loginFailMessage}{" "}
      <a href="/">Go to login page to try again</a>
    </div>
  ) : (
    <div>Logging with Keycloak, please wait...</div>
  );
}

export default withRouter(LoginGooglePage);
