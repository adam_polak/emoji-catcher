import React from "react";
import "./LoginPage.css";
import { Button } from "antd";
import httpClient from "../../tools/httpClient";

export class LoginPage extends React.Component {
  login = () => {
    httpClient.login();
  };
  render() {
    return (
      <div className="login-page">
        <div className="login-form">
          <img
            className="login-logo"
            alt="tsh-logo"
            src={`https://raw.githubusercontent.com/TheSoftwareHouse/kakunin-cli/HEAD/data/tsh.png`}
          />
          <Button
            type="primary"
            className="login-google-btn"
            href={process.env.REACT_APP_KEYCLOAK_AUTH_URL}
          >
            <b>Login with Keycloak</b>
          </Button>
          <div className="powered-by">
            <p>Powered by RAD Modules</p>
          </div>
        </div>
      </div>
    );
  }
}
