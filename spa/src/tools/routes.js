import { Route, Redirect } from "react-router-dom";
import React from "react";

export const PrivateRoute = ({ component: Component, ...rest }) => {
  const accessToken = localStorage.getItem("accessToken");
  return (
    <Route
      {...rest}
      render={props =>
        accessToken ? <Component {...props} /> : <Redirect to="/" />
      }
    />
  );
};
