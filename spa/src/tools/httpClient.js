import axios from "axios";

class HttpClient {
  constructor() {
    this.httpClient = axios.create({
      baseURL: process.env.REACT_APP_BACKEND_URL,
    });

    this.httpClient.interceptors.request.use(
      async (config) => {
        const token = localStorage.getItem("accessToken");
        if (token) config.headers.Authorization = `Bearer ${token}`;
        return Promise.resolve(config);
      },
      (error) => Promise.reject(error)
    );

    this.httpClient.interceptors.response.use(
      async (response) => Promise.resolve(response.data),
      async (error) => {
        // return;
        const originalRequest = error.config;
        if (!error.response || originalRequest._retry) {
          return Promise.reject(error);
        }
        const status = error.response.status;
        if (status !== 401) return Promise.reject(error);

        originalRequest._retry = true;

        return axios
          .post(
            `${process.env.REACT_APP_SECURITY_API_URL}/users/refresh-token`,
            {
              accessToken: localStorage.getItem("accessToken"),
              refreshToken: localStorage.getItem("refreshToken"),
            }
          )
          .then((result) => {
            const { accessToken, refreshToken } = result.data;
            localStorage.setItem("accessToken", accessToken);
            localStorage.setItem("refreshToken", refreshToken);
            originalRequest.headers.Authorization = `Bearer ${accessToken}`;
            return axios(originalRequest);
          });
      }
    );
  }

  getReactions() {
    return this.httpClient.get("/reactions");
  }
  //AUTH

  loginWithKeycloak(code) {
    return axios
      .get(
        `${process.env.REACT_APP_SECURITY_API_URL}/users/oauth-redirect/keycloak?code=${code}&redirectUrl=https://emoji.tsh.dev/login-callback/keycloak`
      )
      .then((res) => res.data);
  }

  isAuthenticated() {
    const token = localStorage.getItem("accessToken");
    if (!token) return Promise.reject(false);
    return axios.get(
      `${process.env.REACT_APP_SECURITY_API_URL}/users/is-authenticated`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
        },
      }
    );
  }

  isAuthenticatedWithRetry() {
    return this.isAuthenticated()
      .then((result) => {
        if (result.data) return result.data;
        return this.refreshToken();
      })
      .then(this.isAuthenticated);
  }

  refreshToken() {
    const refreshToken = localStorage.getItem("refreshToken");
    if (!refreshToken) return Promise.resolve();
    return axios
      .post(`${process.env.REACT_APP_SECURITY_API_URL}/users/refresh-token`, {
        accessToken: localStorage.getItem("accessToken"),
        refreshToken: localStorage.getItem("refreshToken"),
      })
      .then((result) => {
        const { accessToken, refreshToken } = result.data;
        localStorage.setItem("accessToken", accessToken);
        localStorage.setItem("refreshToken", refreshToken);
      });
  }
}

export default new HttpClient();
