const GROUPS = {
  HEADS: "heads",
  DEVS: "devs"
};

const specializationToGroupMap = {
  HEADS: GROUPS.HEADS
};

const groups = {
  [GROUPS.HEADS]: [],
  pm: []
};

const membersApi = [
  {
    id: "ASDASDs",
    specialization: "HEADS"
  }
];

const membersPerGroup = membersApi.reduce((members, member) => {
  const newMembers = { ...members };
  const memberGroup =
    specializationToGroupMap[member.specialization] ?? GROUPS.DEVS;

  newMembers[memberGroup].push({
    ...member,
    specialization: memberGroup
  });

  return newMembers;
}, groups);
