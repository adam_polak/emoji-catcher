class ReactionsRepository {
  constructor(connection) {
    this.connection = connection;
  }

  async increaseReaction(reaction) {
    const document = await this.connection.collection("reactions").findOne();
    if (!document) {
      await this.connection.collection("reactions").insertOne({
        [reaction]: 1
      });

      return { emoji: reaction, count: 1 };
    }

    if (document[reaction]) {
      document[reaction]++;
    } else {
      document[reaction] = 1;
    }

    await this.connection.collection("reactions").updateOne(
      { _id: document._id },
      {
        $set: document
      }
    );

    return { emoji: reaction, count: document[reaction] };
  }

  async decreaseReaction(reaction) {
    const document = await this.connection.collection("reactions").findOne();
    if (document) {
      if (document[reaction]) {
        document[reaction]--;
      }

      await this.connection.collection("reactions").updateOne(
        { _id: document._id },
        {
          $set: document
        }
      );

      return { emoji: reaction, count: document[reaction] };
    }

    return;
  }

  findAll() {
    return this.connection.collection("reactions").findOne();
  }
}

module.exports = ReactionsRepository;
