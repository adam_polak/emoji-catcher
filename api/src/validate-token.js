const { UNAUTHORIZED } = require("http-status-codes");
const fetch = require("node-fetch");
const BearerToken = require("./shared/tokens/bearer.token");

const validateToken = () => async (req, res, next) => {
  const { authorization } = req.headers;
  const isTokenValid = BearerToken.isValid(req.headers.authorization);
  if (!isTokenValid) return res.status(UNAUTHORIZED).json("Token is not valid");

  const { isAuthenticated } = await fetch(
    `${process.env.SECURITY_API_URL}/users/is-authenticated`,
    {
      method: "GET",
      headers: {
        Authorization: authorization
      }
    }
  ).then(result => result.json());

  if (!isAuthenticated)
    return res.status(UNAUTHORIZED).json("Token is not valid");

  return next();
};

module.exports = {
  validateToken
};
