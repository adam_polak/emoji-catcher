class BearerToken {
  static isValid(token) {
    const regex = /^(Bearer) ([^\s]+)$/;

    if (!token || !regex.test(token)) {
      return false;
    }

    const match = token.match(regex);

    if (!match || match.length !== 3) {
      return false;
    }
    return true;
  }
}

module.exports = BearerToken;
