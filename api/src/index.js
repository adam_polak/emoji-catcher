require("dotenv-safe").config({
  example: ".env.dist",
});

const url = `mongodb://${process.env.MONGO_INITDB_USERNAME}:${process.env.MONGO_INITDB_PASSWORD}@localhost:27007/${process.env.MONGO_INITDB_DATABASE}`;

const MongoClient = require("mongodb").MongoClient;
const { WebClient } = require("@slack/web-api");
const SlackClient = require("./slack.client");
const express = require("express");
const cors = require("cors");
const ReactionsRepository = require("./reactions.repository");
const WebSocket = require("ws");
const { parse } = require("url");
const fetch = require("node-fetch");

const fs = require("fs");
const path = require("path");
const { validateToken } = require("./validate-token");

const slackClient = new SlackClient(new WebClient(process.env.SLACK_API_TOKEN));

const rawdata = fs.readFileSync(path.resolve("src/emoji.json"));
const emojiJson = JSON.parse(rawdata);

let emojis = emojiJson.reduce((result, current) => {
  return {
    ...result,
    [current.short_name]: {
      custom: false,
      url: `https://a.slack-edge.com/production-standard-emoji-assets/10.2/apple-medium/${current.unified.toLowerCase()}@2x.png`,
    },
  };
}, {});

const blacklist = [];

MongoClient.connect(url, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
}).then(async (client) => {
  // in case we want to allow our emojis to be visible
  const customEmojis = await slackClient.listEmojis();
  const allowedEmojis = Object.keys(customEmojis.emoji).reduce((keys, key) => {
    return {
      ...keys,
      [key]: {
        custom: true,
        url: customEmojis.emoji[key],
      },
    };
  }, {});

  emojis = {
    ...emojis,
    ...allowedEmojis,
  };

  const db = client.db("tsh-emoji");
  const reactionsRepository = new ReactionsRepository(db);

  const app = express();
  const wss = new WebSocket.Server({ port: 5001 });

  wss.on("connection", async (socket, request) => {
    const parsedUrl = parse(request.url, true);
    const { accessToken } = parsedUrl.query;

    console.log({ accessToken });

    const { isAuthenticated } = await fetch(
      `${process.env.SECURITY_API_URL}/users/is-authenticated`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }
    ).then((result) => result.json());

    console.log({ isAuthenticated });

    if (!isAuthenticated) {
      console.log("TERMINATEDs");
      socket.terminate();
    }
  });

  app.use(cors());
  app.use(express.json());

  app.post("/event", async (req, res) => {
    const event = req.body;

    if (event.type === "url_verification") {
      res.status(200).json({
        challenge: event.challenge,
      });
      return;
    }

    if (event.type === "event_callback") {
      console.log({ event });
      if (event.event.type === "reaction_added") {
        const reaction = await reactionsRepository.increaseReaction(
          event.event.reaction
        );

        if (emojis[reaction.emoji]) {
          wss.clients.forEach(function each(client) {
            if (client.readyState === WebSocket.OPEN) {
              client.send(
                JSON.stringify({
                  ...reaction,
                  url: emojis[reaction.emoji].url,
                  type: "reaction_added",
                })
              );
            }
          });
        }
      }

      if (event.event.type === "reaction_removed") {
        const reaction = await reactionsRepository.decreaseReaction(
          event.event.reaction
        );

        console.log({ tmp: emojis[reaction.emoji] });
        console.log(wss.clients.size);

        if (reaction && emojis[reaction.emoji]) {
          wss.clients.forEach(function each(client) {
            console.log({ reaction });
            if (client.readyState === WebSocket.OPEN) {
              client.send(
                JSON.stringify({
                  ...reaction,
                  url: emojis[reaction.emoji].url,
                  type: "reaction_removed",
                })
              );
            }
          });
        }
      }
    }

    res.status(200).send();
  });

  app.get("/reactions", [validateToken()], async (req, res) => {
    const reactions = await reactionsRepository.findAll();

    const onlyReactions = {
      ...reactions,
    };

    delete onlyReactions._id;

    const unsortedReactions = Object.entries(onlyReactions).map((entry) => ({
      emoji: entry[0],
      count: entry[1],
    }));

    const sortedReactions = unsortedReactions
      .slice()
      .sort((a, b) => b.count - a.count);

    res.json(
      sortedReactions
        .filter((reaction) => {
          return !!emojis[reaction.emoji];
        })
        .map((reaction) => ({
          ...reaction,
          url: emojis[reaction.emoji].url,
        }))
    );
  });

  app.listen(5000);
  console.log("APP READY");
});
