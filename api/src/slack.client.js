class SlackClient {
  constructor(webClient) {
    this.webClient = webClient;
  }

  listEmojis() {
    return this.webClient.emoji.list();
  }
}

module.exports = SlackClient;
